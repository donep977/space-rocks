{
    "id": "f1740c47-6077-40e4-a53e-c25781a80c38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2264c6cc-d9c7-4549-b34c-fd161b7756a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1740c47-6077-40e4-a53e-c25781a80c38",
            "compositeImage": {
                "id": "07bd481b-53a4-41af-952e-8f5f35209f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2264c6cc-d9c7-4549-b34c-fd161b7756a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ab34da5-9add-402b-9e46-e2e99da186e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2264c6cc-d9c7-4549-b34c-fd161b7756a5",
                    "LayerId": "fe7fb41e-5c4b-4ebe-80ab-1a91839841ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "fe7fb41e-5c4b-4ebe-80ab-1a91839841ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1740c47-6077-40e4-a53e-c25781a80c38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}