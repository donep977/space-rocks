{
    "id": "687a6b7f-4c29-4f1f-8e62-301fada6c87b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc180917-e5d3-4365-933e-060163d2875d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "687a6b7f-4c29-4f1f-8e62-301fada6c87b",
            "compositeImage": {
                "id": "9944b6d6-64b5-4ef3-aa41-3597ac14a384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc180917-e5d3-4365-933e-060163d2875d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d555145-f99b-4a68-92fc-36424eda95eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc180917-e5d3-4365-933e-060163d2875d",
                    "LayerId": "75537aa2-eb09-40ac-bfbb-4698f95ea6bf"
                }
            ]
        }
    ],
    "gridX": 18,
    "gridY": 18,
    "height": 32,
    "layers": [
        {
            "id": "75537aa2-eb09-40ac-bfbb-4698f95ea6bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "687a6b7f-4c29-4f1f-8e62-301fada6c87b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}