{
    "id": "21721357-fe63-4df1-a189-7963ad282b28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5903f779-f92a-4580-bd08-da55e4bce26f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21721357-fe63-4df1-a189-7963ad282b28",
            "compositeImage": {
                "id": "12548ba1-dad3-4363-a8f0-6a3d39681a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5903f779-f92a-4580-bd08-da55e4bce26f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207757ec-f981-4651-969a-ec7c28e4c035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5903f779-f92a-4580-bd08-da55e4bce26f",
                    "LayerId": "ae225232-816c-4269-9908-227bfb6181f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "ae225232-816c-4269-9908-227bfb6181f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21721357-fe63-4df1-a189-7963ad282b28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}