{
    "id": "d727c13b-db2c-4e82-861a-576f9e956e27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7970f804-c8af-493e-916f-e3e96cfdcd90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d727c13b-db2c-4e82-861a-576f9e956e27",
            "compositeImage": {
                "id": "a605605a-2c0d-421b-921b-26db37880f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7970f804-c8af-493e-916f-e3e96cfdcd90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a8385c2-c65d-4e36-918f-bf273218f3f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7970f804-c8af-493e-916f-e3e96cfdcd90",
                    "LayerId": "46a54213-df7e-40fe-94b8-110e219f60dc"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 16,
    "layers": [
        {
            "id": "46a54213-df7e-40fe-94b8-110e219f60dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d727c13b-db2c-4e82-861a-576f9e956e27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}