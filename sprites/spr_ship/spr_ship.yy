{
    "id": "79efb42f-c8a0-4be2-87a3-2a6b2e2c5061",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 11,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "496c4dda-6d87-4c2c-b8c2-0272b29ada08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79efb42f-c8a0-4be2-87a3-2a6b2e2c5061",
            "compositeImage": {
                "id": "c4fc61ed-b80f-42b0-80de-ef5cb5ffa8f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "496c4dda-6d87-4c2c-b8c2-0272b29ada08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a507d27-42f3-47d7-9385-cce7fbe2d460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "496c4dda-6d87-4c2c-b8c2-0272b29ada08",
                    "LayerId": "f228cf52-5cd2-4306-be67-7004d1af87bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f228cf52-5cd2-4306-be67-7004d1af87bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79efb42f-c8a0-4be2-87a3-2a6b2e2c5061",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}