{
    "id": "6f255b10-6510-4408-813d-779a4c18b685",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 7,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a1ebb64-66af-4158-9565-575ad8d79dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f255b10-6510-4408-813d-779a4c18b685",
            "compositeImage": {
                "id": "5488597a-b0d3-4e89-801d-f4e88e8630be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a1ebb64-66af-4158-9565-575ad8d79dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d0b1e11-6dc1-4f61-8be5-00e6b59486d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a1ebb64-66af-4158-9565-575ad8d79dee",
                    "LayerId": "69beecd3-01fc-442e-a42c-07df37b049ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "69beecd3-01fc-442e-a42c-07df37b049ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f255b10-6510-4408-813d-779a4c18b685",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}