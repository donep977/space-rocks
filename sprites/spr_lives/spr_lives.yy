{
    "id": "f2438403-79c6-4ca8-b138-ff6feaae1a68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 18,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5762e6d7-34cc-4774-8ed7-bdacd24549c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2438403-79c6-4ca8-b138-ff6feaae1a68",
            "compositeImage": {
                "id": "55b2475c-ae1b-47eb-8d6a-79a656149435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5762e6d7-34cc-4774-8ed7-bdacd24549c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6527cf07-5893-4a9a-9234-39134bcc45cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5762e6d7-34cc-4774-8ed7-bdacd24549c8",
                    "LayerId": "9d7d38c0-9278-4f29-a92b-10661c577ee9"
                }
            ]
        }
    ],
    "gridX": 6,
    "gridY": 6,
    "height": 24,
    "layers": [
        {
            "id": "9d7d38c0-9278-4f29-a92b-10661c577ee9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2438403-79c6-4ca8-b138-ff6feaae1a68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}