{
    "id": "6d197ddf-7fbc-4e13-85ce-ede2a5923ec0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_asteroid",
    "eventList": [
        {
            "id": "af7c12ed-680e-4434-8daf-852748f846a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d197ddf-7fbc-4e13-85ce-ede2a5923ec0"
        },
        {
            "id": "6bf6300b-3b5d-48b9-9838-ebf042d56fa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d197ddf-7fbc-4e13-85ce-ede2a5923ec0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d727c13b-db2c-4e82-861a-576f9e956e27",
    "visible": true
}