/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
/// @DnDVersion : 1
/// @DnDHash : 05A1C5E2
/// @DnDArgument : "x" "20"
/// @DnDArgument : "y" "20"
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
draw_text(20, 20, string("Score: ") + string(__dnd_score));

/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
/// @DnDVersion : 1
/// @DnDHash : 26219127
/// @DnDArgument : "x" "20"
/// @DnDArgument : "y" "40"
/// @DnDArgument : "sprite" "spr_lives"
/// @DnDSaveInfo : "sprite" "f2438403-79c6-4ca8-b138-ff6feaae1a68"
var l26219127_0 = sprite_get_width(spr_lives);
var l26219127_1 = 0;
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
for(var l26219127_2 = __dnd_lives; l26219127_2 > 0; --l26219127_2) {
	draw_sprite(spr_lives, 0, 20 + l26219127_1, 40);
	l26219127_1 += l26219127_0;
}