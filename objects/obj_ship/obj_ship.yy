{
    "id": "c6edb424-8a09-4e3c-9cea-f9483f5e1086",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "ca37eeb8-af80-4002-b7f8-240cc00a833c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6edb424-8a09-4e3c-9cea-f9483f5e1086"
        },
        {
            "id": "79394331-2505-4bf7-911b-8148b0e951ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "c6edb424-8a09-4e3c-9cea-f9483f5e1086"
        },
        {
            "id": "6a81f5a8-e915-48a0-b1dc-fc21131d4e6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "c6edb424-8a09-4e3c-9cea-f9483f5e1086"
        },
        {
            "id": "4248649d-0739-4152-9476-a804e4003ef6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "c6edb424-8a09-4e3c-9cea-f9483f5e1086"
        },
        {
            "id": "8ce10e69-f9a1-49e8-ac57-96c6edc55cb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6d197ddf-7fbc-4e13-85ce-ede2a5923ec0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c6edb424-8a09-4e3c-9cea-f9483f5e1086"
        },
        {
            "id": "cc10501c-874e-41ba-a9a1-0db69ac3bec7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "c6edb424-8a09-4e3c-9cea-f9483f5e1086"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "79efb42f-c8a0-4be2-87a3-2a6b2e2c5061",
    "visible": true
}