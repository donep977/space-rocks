/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 25BD1D78
instance_destroy();

/// @DnDAction : YoYo Games.Common.Apply_To
/// @DnDVersion : 1
/// @DnDHash : 16EDA685
/// @DnDApplyTo : other
with(other) {

}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 2A3C39F9
instance_destroy();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 77B53117
/// @DnDArgument : "var" "sprite_index"
/// @DnDArgument : "value" "spr_asteroid_huge"
if(sprite_index == spr_asteroid_huge)
{

}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 7BFCA358
/// @DnDArgument : "var" "sprite_index"
/// @DnDArgument : "value" "spr_asteroid_med"
if(sprite_index == spr_asteroid_med)
{

}

/// @DnDAction : YoYo Games.Loops.Repeat
/// @DnDVersion : 1
/// @DnDHash : 2AD991F5

{

}

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 634E50DE
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "var" "NewAsteroid"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "objectid" "obj_asteroid"
/// @DnDSaveInfo : "objectid" "6d197ddf-7fbc-4e13-85ce-ede2a5923ec0"
var NewAsteroid = instance_create_layer(x + 0, y + 0, "Instances", obj_asteroid);

/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 31CDA9BA
/// @DnDArgument : "expr" "spr_asteroid_small"
/// @DnDArgument : "var" "NewAsteroid.sprite_index"
NewAsteroid.sprite_index = spr_asteroid_small;

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 54118ED3
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "objectid" "obj_debris"
/// @DnDSaveInfo : "objectid" "85994f2f-5704-4914-b1b4-d6a4c7b6504a"
instance_create_layer(x + 0, y + 0, "Instances", obj_debris);