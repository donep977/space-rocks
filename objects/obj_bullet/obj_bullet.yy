{
    "id": "accc9b7f-0aa8-4bfd-b2c2-cc159655192f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "e736dd8c-9236-462d-bd24-c8b55a5c2dde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "accc9b7f-0aa8-4bfd-b2c2-cc159655192f"
        },
        {
            "id": "e17fac1c-ec41-4e70-9d39-835d46becff6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "6d197ddf-7fbc-4e13-85ce-ede2a5923ec0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "accc9b7f-0aa8-4bfd-b2c2-cc159655192f"
        },
        {
            "id": "a39c8c0b-3d89-46e9-abd6-adf94e49b410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "accc9b7f-0aa8-4bfd-b2c2-cc159655192f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "21721357-fe63-4df1-a189-7963ad282b28",
    "visible": true
}